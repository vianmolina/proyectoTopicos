﻿using System.Threading.Tasks;
using Ulatina.Selectos.Proyecto.GenericRepository.Model.Dominio.Interfaces;

namespace Ulatina.Selectos.Proyecto.GenericRepository.Dominio.Interfaces
{
    public interface IRepository : IReadOnlyRepository
    {
        void Create<TEntity>(TEntity entity, string createdBy = null)
            where TEntity : class, IEntity;

        void Update<TEntity>(TEntity entity, string modifiedBy = null)
            where TEntity : class, IEntity;

        void Delete<TEntity>(object id)
            where TEntity : class, IEntity;

        void Delete<TEntity>(TEntity entity)
            where TEntity : class, IEntity;

        void Save();

        Task SaveAsync();
    }
}