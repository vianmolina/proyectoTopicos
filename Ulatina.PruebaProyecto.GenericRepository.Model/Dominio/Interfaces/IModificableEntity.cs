﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulatina.Selectos.Proyecto.GenericRepository.Model.Dominio.Interfaces
{
    public interface IModifiableEntity
    {
        string _Name { get; set; }
    }
}
