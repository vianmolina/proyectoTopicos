﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL.Acciones
{
    public class Adjuntos
    {
        public DbContext miContexto;
        private readonly string _includeTables = string.Empty;
        private readonly string _noIncludeTables = string.Empty;

        public Adjuntos()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }

        public Adjuntos(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Especificaciones.Adjuntos ObtenerRepositorio()
        {
            Especificaciones.Adjuntos result;
            if (miContexto == null)
                result = new Especificaciones.Adjuntos();
            else
                result = new Especificaciones.Adjuntos(miContexto);
            return result;
        }


        public File CrearRegistro(File registro)
        {
            File elResultado;
            Especificaciones.Adjuntos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.AgregarNuevoRegistro(registro);

            return elResultado;
        }
        public File ActualizarRegistro(File registro)
        {
            File elResultado;
            Especificaciones.Adjuntos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ActualizarResgistro(registro);

            return elResultado;
        }

        public File BuscarRegistroPorCodigo(int codigo)
        {
            File elResultado;
            Especificaciones.Adjuntos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.BuscarRegistroPorCodigo(codigo);

            return elResultado;
        }
        public List<File> ObtenerTodos()
        {
            List<File> elResultado;
            Especificaciones.Adjuntos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ObtenerTodos();

            return elResultado;
        }
    }
}