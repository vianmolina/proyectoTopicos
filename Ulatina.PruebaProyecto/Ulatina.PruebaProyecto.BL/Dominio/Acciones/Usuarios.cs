﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL.Acciones
{
    public class Usuarios
    {
        public DbContext miContexto;

        public Usuarios()
        {
        }

        public Usuarios(DbContext elContexto)
        {
            miContexto = elContexto;
        }
        private Especificaciones.Usuarios ObtenerRepositorio()
        {
            Especificaciones.Usuarios result;
            if (miContexto == null)
                result = new Especificaciones.Usuarios();
            else
                result = new Especificaciones.Usuarios(miContexto);
            return result;
        }

        public UserInfo CrearUsuario(UserInfo registro)
        {
            UserInfo elResultado;
            Especificaciones.Usuarios laEspecificacion = ObtenerRepositorio();
           
            elResultado = laEspecificacion.AgregarNuevoRegistro(registro);

            return elResultado;
        }
        public UserInfo ActualizarRegistro(UserInfo registro)
        {
            UserInfo elResultado;
            Especificaciones.Usuarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ActualizarResgistro(registro);

            return elResultado;
        }

        public UserInfo BuscarRegistroPorCodigo(int codigo)
        {
            UserInfo elResultado;
            Especificaciones.Usuarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.BuscarRegistroPorCodigo(codigo);

            return elResultado;
        }
        public List<UserInfo> ObtenerTodos()
        {
            List<UserInfo> elResultado;
            Especificaciones.Usuarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ObtenerTodos();

            return elResultado;
        }
        public List<AspNetRoles> ObtenerTodosLosRoles()
        {
            List<AspNetRoles> elResultado;
            Especificaciones.Usuarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ObtenerTodosLosRoles();

            return elResultado;
        }
    }
}