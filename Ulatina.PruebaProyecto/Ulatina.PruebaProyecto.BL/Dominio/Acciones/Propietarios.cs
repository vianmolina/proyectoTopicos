﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL.Acciones
{
    public class Propietarios
    {
        public DbContext miContexto;

        public Propietarios()
        {
        }

        public Propietarios(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Especificaciones.Propietarios ObtenerRepositorio()
        {
            Especificaciones.Propietarios result;
            if (miContexto == null)
                result = new Especificaciones.Propietarios();
            else
                result = new Especificaciones.Propietarios(miContexto);
            return result;
        }


        public Owner CrearRegistro(Owner registro)
        {
            Owner elResultado;
            Especificaciones.Propietarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.AgregarNuevoRegistro(registro);

            return elResultado;
        }
        public Owner ActualizarRegistro(Owner registro)
        {
            Owner elResultado;
            Especificaciones.Propietarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ActualizarResgistro(registro);

            return elResultado;
        }

        public Owner BuscarRegistroPorCodigo(int codigo)
        {
            Owner elResultado;
            Especificaciones.Propietarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.BuscarRegistroPorCodigo(codigo);

            return elResultado;
        }
        public List<Owner> ObtenerTodos()
        {
            List<Owner> elResultado;
            Especificaciones.Propietarios laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ObtenerTodos();

            return elResultado;
        }
    }
}