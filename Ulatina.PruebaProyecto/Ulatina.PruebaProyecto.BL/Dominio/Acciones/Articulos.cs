﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL.Acciones
{
    public class Articulos
    {
        public DbContext miContexto;

        public Articulos()
        {
        }

        public Articulos(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Especificaciones.Articulos ObtenerRepositorio()
        {
            Especificaciones.Articulos result;
            if (miContexto == null)
                result = new Especificaciones.Articulos();
            else
                result = new Especificaciones.Articulos(miContexto);
            return result;
        }


        public Item CrearRegistro(Item registro)
        {
            Item elResultado;
            Especificaciones.Articulos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.AgregarNuevoRegistro(registro);

            return elResultado;
        }
        public Item ActualizarRegistro(Item registro)
        {
            Item elResultado;
            Especificaciones.Articulos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ActualizarResgistro(registro);

            return elResultado;
        }

        public Item BuscarRegistroPorCodigo(int codigo)
        {
            Item elResultado;
            Especificaciones.Articulos laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.BuscarRegistroPorCodigo(codigo);

            return elResultado;
        }
        public List<Item> ObtenerTodos()
        {
            List<Item> elResultado;
            Especificaciones.Articulos laEspecificacion = ObtenerRepositorio();
               elResultado = laEspecificacion.ObtenerTodos();
            
            return elResultado;
        }
    }
}