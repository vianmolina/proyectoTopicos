﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL.Acciones
{
    public class Etiquetas
    {
        public DbContext miContexto;

        public Etiquetas()
        {
        }

        public Etiquetas(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Especificaciones.Etiquetas ObtenerRepositorio()
        {
            Especificaciones.Etiquetas result;
            if (miContexto == null)
                result = new Especificaciones.Etiquetas();
            else
                result = new Especificaciones.Etiquetas(miContexto);
            return result;
        }


        public Tag CrearRegistro(Tag registro)
        {
            Tag elResultado;
            Especificaciones.Etiquetas laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.AgregarNuevoRegistro(registro);

            return elResultado;
        }
        public Tag ActualizarRegistro(Tag registro)
        {
            Tag elResultado;
            Especificaciones.Etiquetas laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ActualizarResgistro(registro);

            return elResultado;
        }

        public Tag BuscarRegistroPorCodigo(int codigo)
        {
            Tag elResultado;
            Especificaciones.Etiquetas laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.BuscarRegistroPorCodigo(codigo);

            return elResultado;
        }
        public List<Tag> ObtenerTodos()
        {
            List<Tag> elResultado;
            Especificaciones.Etiquetas laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ObtenerTodos();

            return elResultado;
        }
    }
}