﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL.Acciones
{
    public class Parametros
    {
        public DbContext miContexto;

        public Parametros()
        {
        }

        public Parametros(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Especificaciones.Parametros ObtenerRepositorio()
        {
            Especificaciones.Parametros result;
            if (miContexto == null)
                result = new Especificaciones.Parametros();
            else
                result = new Especificaciones.Parametros(miContexto);
            return result;
        }

        public Parameter CrearRegistro(Parameter registro)
        {
            Parameter elResultado;
            Especificaciones.Parametros laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.AgregarNuevoRegistro(registro);

            return elResultado;
        }
        public Parameter ActualizarRegistro(Parameter registro)
        {
            Parameter elResultado;
            Especificaciones.Parametros laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ActualizarResgistro(registro);

            return elResultado;
        }

        public Parameter BuscarRegistroPorCodigo(int codigo)
        {
            Parameter elResultado;
            Especificaciones.Parametros laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.BuscarRegistroPorCodigo(codigo);

            return elResultado;
        }
        public List<Parameter> ObtenerTodos()
        {
            List<Parameter> elResultado;
            Especificaciones.Parametros laEspecificacion = ObtenerRepositorio();

            elResultado = laEspecificacion.ObtenerTodos();

            return elResultado;
        }
    }
}