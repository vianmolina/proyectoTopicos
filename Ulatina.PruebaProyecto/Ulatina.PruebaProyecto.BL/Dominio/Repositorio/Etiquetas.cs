﻿using System.Collections.Generic;
using System.Data.Entity;
using Ulatina.PruebaProyecto.GenericRepository.Dominio.Servicios;
using Ulatina.PruebaProyecto.Model;
using System.Linq;

namespace Ulatina.PruebaProyecto.BL.Dominio.Repositorio
{
    public class Etiquetas
    {
        private static DbContext miContexto;
        private readonly EntityFrameworkRepository<DbContext> Repository;

        private readonly string _includeTables = string.Empty;
        private readonly string _noIncludeTables = string.Empty;

        #region Constructor
        public Etiquetas()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        public Etiquetas(DbContext elContexto)
        {
            miContexto = elContexto;
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        #endregion
        #region Consultas
        internal Tag Agregar(Tag registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Create(registro);
            Repository.Save();
            return registro;
        }
        internal Tag Actualizar(Tag registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Update(registro);
            Repository.Save();
            return registro;
        }
        internal Tag BuscarPorCodigo(int codigo)
        {
            Tag registro = Repository.GetById<Tag>(codigo);
            return registro;
        }
        internal List<Tag> ObtenerTodos()
        {
            ICollection<Tag> registro = Repository.GetAll<Tag>();
            return registro.ToList();
        }
        #endregion
    }
}