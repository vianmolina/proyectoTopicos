﻿using System.Collections.Generic;
using System.Data.Entity;
using Ulatina.PruebaProyecto.GenericRepository.Dominio.Servicios;
using Ulatina.PruebaProyecto.Model;
using System.Linq;

namespace Ulatina.PruebaProyecto.Dominio.Repositorio
{
    public class Parametros
    {
        private static DbContext miContexto;
        private readonly EntityFrameworkRepository<DbContext> Repository;

        private readonly string _includeTables = string.Empty;
        private readonly string _noIncludeTables = string.Empty;

        #region Constructor
        public Parametros()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        public Parametros(DbContext elContexto)
        {
            miContexto = elContexto;
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        #endregion 
        #region Consultas
        internal Parameter Agregar(Parameter registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Create(registro);
            Repository.Save();
            return registro;
        }
        internal Parameter Actualizar(Parameter registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Update(registro);
            Repository.Save();
            return registro;
        }
        internal Parameter BuscarPorCodigo(int codigo)
        {
            Parameter registro = Repository.GetById<Parameter>(codigo);
            return registro;
        }
        internal List<Parameter> ObtenerTodos()
        {
            ICollection<Parameter> registro = Repository.GetAll<Parameter>();
            return registro.ToList();
        }
        #endregion
    }
}