﻿using System.Collections.Generic;
using System.Data.Entity;
using Ulatina.PruebaProyecto.GenericRepository.Dominio.Servicios;
using Ulatina.PruebaProyecto.Model;
using System.Linq;

namespace Ulatina.PruebaProyecto.BL.Dominio.Repositorio
{
    public class Adjuntos
    {
        private static DbContext miContexto;
        private readonly EntityFrameworkRepository<DbContext> Repository;

        private readonly string _includeTables = string.Empty;
        private readonly string _noIncludeTables = string.Empty;

        #region Constructor
        public Adjuntos()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        public Adjuntos(DbContext elContexto)
        {
            miContexto = elContexto;
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        #endregion
        #region Consultas
        internal File Agregar(File registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Create(registro);
            Repository.Save();
            return registro;
        }
        internal File Actualizar(File registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Update(registro);
            Repository.Save();
            return registro;
        }
        internal File BuscarPorCodigo(int codigo)
        {
            File registro = Repository.GetById<File>(codigo);
            return registro;
        }
        internal List<File> ObtenerTodos()
        {
            ICollection<File> registro = Repository.GetAll<File>();
            return registro.ToList();
        }
        #endregion
    }
}