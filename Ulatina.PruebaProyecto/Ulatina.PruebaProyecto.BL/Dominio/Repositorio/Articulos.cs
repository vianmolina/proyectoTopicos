﻿using System.Collections.Generic;
using System.Data.Entity;
using Ulatina.PruebaProyecto.GenericRepository.Dominio.Servicios;
using Ulatina.PruebaProyecto.Model;
using System.Linq;

namespace Ulatina.PruebaProyecto.BL.Dominio.Repositorio
{
    public class Articulos
    {
        private static DbContext miContexto;
        private readonly EntityFrameworkRepository<DbContext> Repository;

        private readonly string _includeTables = string.Empty;
        private readonly string _noIncludeTables = string.Empty;

        #region Constructor
        public Articulos()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        public Articulos(DbContext elContexto)
        {
            miContexto = elContexto;
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        #endregion
        #region Consultas
        internal Item Agregar(Item registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Create(registro);
            Repository.Save();
            return registro;
        }
        internal Item Actualizar(Item registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Update(registro);
            Repository.Save();
            return registro;
        }
        internal Item BuscarPorCodigo(int codigo)
        {
            Item registro = Repository.GetById<Item>(codigo);
            return registro;
        }
        internal List<Item> ObtenerTodos()
        {
            ICollection<Item> registro = Repository.GetAll<Item>();
            return registro.ToList();
        }
        #endregion
    }
}