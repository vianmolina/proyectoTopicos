﻿using System.Collections.Generic;
using System.Data.Entity;
using Ulatina.PruebaProyecto.GenericRepository.Dominio.Servicios;
using Ulatina.PruebaProyecto.Model;
using System.Linq;

namespace Ulatina.PruebaProyecto.Dominio.Repositorio
{
    public class Propietarios
    {
        private static DbContext miContexto;
        private readonly EntityFrameworkRepository<DbContext> Repository;

        private readonly string _includeTables = string.Empty;
        private readonly string _noIncludeTables = string.Empty;

        #region Constructor
        public Propietarios()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        public Propietarios(DbContext elContexto)
        {
            miContexto = elContexto;
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        #endregion  
        #region Consultas
        internal Owner Agregar(Owner registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Create(registro);
            Repository.Save();
            return registro;
        }
        internal Owner Actualizar(Owner registro)
        {
            // para usar el .ToList(), se requiere haber incluido el System.Linq;
            Repository.Update(registro);
            Repository.Save();
            return registro;
        }
        internal Owner BuscarPorCodigo(int codigo)
        {
            Owner registro = Repository.GetById<Owner>(codigo);
            return registro;
        }
        internal List<Owner> ObtenerTodos()
        {
            ICollection<Owner> registro = Repository.GetAll<Owner>();
            return registro.ToList();
        }
        #endregion
    }
}