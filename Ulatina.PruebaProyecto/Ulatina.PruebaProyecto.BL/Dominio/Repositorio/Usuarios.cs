﻿using System.Collections.Generic;
using System.Data.Entity;
using Ulatina.PruebaProyecto.GenericRepository.Dominio.Servicios;
using Ulatina.PruebaProyecto.Model;
using System.Linq;

namespace Ulatina.PruebaProyecto.Dominio.Repositorio
{
    public class Usuarios
    {
        private static DbContext miContexto;
        private readonly EntityFrameworkRepository<DbContext> Repository;

        private readonly string _includeTables = "AspNetUsers,AspNetUsers.AspNetRoles";
        private readonly string _noIncludeTables = string.Empty;

        #region Constructor
        public Usuarios()
        {
            miContexto = new ProyectoTopicosSelectosEntities();
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        public Usuarios(DbContext elContexto)
        {
            miContexto = elContexto;
            Repository = new EntityFrameworkRepository<DbContext>(miContexto);
        }
        #endregion 
        #region Consultas
        internal UserInfo Agregar(UserInfo registro)
        {
            Repository.Create(registro);
            Repository.Save();
            return registro;
        }
        internal UserInfo Actualizar(UserInfo registro)
        {
            UserInfo registroUsuario = Repository.GetById<UserInfo>(registro.Id);
            registroUsuario.FirstName = registro.FirstName;
            registroUsuario.LastName = registro.LastName;
            registroUsuario.Telefono = registro.Telefono;
            registroUsuario.Status = registro.Status;
            registroUsuario.Email = registro.Email;
            registroUsuario.Identification = registro.Identification;
            //Repository.Update(registro);
            Repository.Save();
            return BuscarPorCodigo(registroUsuario.Id);
        }
        internal UserInfo BuscarPorCodigo(int codigo)
        {
            UserInfo registro = Repository.GetById<UserInfo>(codigo);
            return registro;
        }
        internal List<UserInfo> ObtenerTodos()
        {
            ICollection<UserInfo> registro = Repository.GetAll<UserInfo>(null,_includeTables,null,null);
            return registro.ToList();
        }

        internal List<AspNetRoles> ObtenerTodosLosRoles()
        {
            ICollection<AspNetRoles> registro = Repository.GetAll<AspNetRoles>(null, string.Empty, null, null);
            return registro.ToList();
        }
        internal List<AspNetRoles> ObtenerRolesUsuario(string codigoUsuario)
        {
           var registro = Repository.Get<AspNetUsers>(p => p.Id == codigoUsuario,null, "AspNetRoles", null, null).FirstOrDefault();
            List<AspNetRoles> roles = registro.AspNetRoles.ToList();
            return roles;
        }
        #endregion
    }
}