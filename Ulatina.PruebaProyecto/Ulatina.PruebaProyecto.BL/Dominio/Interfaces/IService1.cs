﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.BL
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        #region Adjuntos

        [OperationContract]
        string AgregarAdjunto(File Adjunto);

        [OperationContract]
        string ActualizarAdjunto(File Adjunto);

        [OperationContract]
        string BuscarAdjuntoPorCodigo(int codigo);

        [OperationContract]
        string ObtenerTodosLosAdjuntos();
        #endregion

        #region Articulos

        [OperationContract]
        string AgregarArticulo(Item Articulo);

        [OperationContract]
        string ActualizarArticulo(Item Articulo);

        [OperationContract]
        string BuscarArticuloPorCodigo(int codigo);

        [OperationContract]
        string ObtenerTodosLosArticulos();
        #endregion

        #region Etiquetas

        [OperationContract]
        string AgregarEtiqueta(Tag Etiqueta);

        [OperationContract]
        string ActualizarEtiqueta(Tag Etiqueta);

        [OperationContract]
        string BuscarEtiquetaPorCodigo(int codigo);

        [OperationContract]
        string ObtenerTodosLosEtiquetas();
        #endregion

        #region Parametros

        [OperationContract]
        string AgregarParametro(Parameter Parametro);

        [OperationContract]
        string ActualizarParametro(Parameter Parametro);

        [OperationContract]
        string BuscarParametroPorCodigo(int codigo);

        [OperationContract]
        string ObtenerTodosLosParametros();
        #endregion

        #region Propietarios

        [OperationContract]
        string AgregarPropietario(Owner Propietario);

        [OperationContract]
        string ActualizarPropietario(Owner Propietario);

        [OperationContract]
        string BuscarPropietarioPorCodigo(int codigo);

        [OperationContract]
        string ObtenerTodosLosPropietarios();

        #endregion

        #region Usuarios

        [OperationContract]
        string AgregarUsuario(UserInfo usuario);

        [OperationContract]
        string ActualizarUsuario(UserInfo usuario);

        [OperationContract]
        string BuscarUsuarioPorCodigo(int codigo);

        [OperationContract]
        string ObtenerTodosLosUsuarios();
        [OperationContract]
        string ObtenerTodosLosRoles();
        #endregion

    }
}
