﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Ulatina.Selectos.Proyecto.Model;

namespace Ulatina.Selectos.Proyecto.Wcf
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        #region Adjuntos

        public string AgregarAdjunto(File adjunto)
        {
            var laAccion = new Dominio.Acciones.Adjuntos();
            var elResultado = JsonConvert.SerializeObject(laAccion.CrearRegistro(adjunto));
            return elResultado;
        }

        public string ActualizarAdjunto(File adjunto)
        {
            var laAccion = new Dominio.Acciones.Adjuntos();
            var elResultado = JsonConvert.SerializeObject(laAccion.ActualizarRegistro(adjunto));
            return elResultado;
        }

        public string BuscarAdjuntoPorCodigo(int codigo)
        {
            var laAccion = new Dominio.Acciones.Adjuntos();
            var elResultado = JsonConvert.SerializeObject(laAccion.BuscarRegistroPorCodigo(codigo));
            return elResultado;
        }

        public string ObtenerTodosLosAdjuntos()
        {
            var laAccion = new Dominio.Acciones.Adjuntos();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodos());
            return elResultado;
        }
        
        #endregion

        #region Articulos

       public string AgregarArticulo(Item articulo)
        {
            var laAccion = new Dominio.Acciones.Articulos();
            var elResultado = JsonConvert.SerializeObject(laAccion.CrearRegistro(articulo));
            return elResultado;
        }

        public string ActualizarArticulo(Item articulo)
        {
            var laAccion = new Dominio.Acciones.Articulos();
            var elResultado = JsonConvert.SerializeObject(laAccion.ActualizarRegistro(articulo));
            return elResultado;
        }

        public string BuscarArticuloPorCodigo(int codigo)
        {
            var laAccion = new Dominio.Acciones.Articulos();
            var elResultado = JsonConvert.SerializeObject(laAccion.BuscarRegistroPorCodigo(codigo));
            return elResultado;
        }

        public string ObtenerTodosLosArticulos()
        {
            var laAccion = new Dominio.Acciones.Articulos();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodos());
            return elResultado;
        }
        #endregion

        #region Etiquetas

        public string AgregarEtiqueta(Tag etiqueta)
        {
            var laAccion = new Dominio.Acciones.Etiquetas();
            var elResultado = JsonConvert.SerializeObject(laAccion.CrearRegistro(etiqueta));
            return elResultado;
        }

        public string ActualizarEtiqueta(Tag etiqueta)
        {
            var laAccion = new Dominio.Acciones.Etiquetas();
            var elResultado = JsonConvert.SerializeObject(laAccion.ActualizarRegistro(etiqueta));
            return elResultado;
        }

        public string BuscarEtiquetaPorCodigo(int codigo)
        {
            var laAccion = new Dominio.Acciones.Etiquetas();
            var elResultado = JsonConvert.SerializeObject(laAccion.BuscarRegistroPorCodigo(codigo));
            return elResultado;
        }

        public string ObtenerTodosLosEtiquetas()
        {
            var laAccion = new Dominio.Acciones.Etiquetas();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodos());
            return elResultado;
        }
        #endregion

        #region Parametros

        public string AgregarParametro(Parameter parametro)
        {
            var laAccion = new Dominio.Acciones.Parametros();
            var elResultado = JsonConvert.SerializeObject(laAccion.CrearRegistro(parametro));
            return elResultado;
        }

        public string ActualizarParametro(Parameter parametro)
        {
            var laAccion = new Dominio.Acciones.Parametros();
            var elResultado = JsonConvert.SerializeObject(laAccion.ActualizarRegistro(parametro));
            return elResultado;
        }

        public string BuscarParametroPorCodigo(int codigo)
        {
            var laAccion = new Dominio.Acciones.Parametros();
            var elResultado = JsonConvert.SerializeObject(laAccion.BuscarRegistroPorCodigo(codigo));
            return elResultado;
        }

        public string ObtenerTodosLosParametros()
        {
            var laAccion = new Dominio.Acciones.Parametros();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodos());
            return elResultado;
        }
        #endregion

        #region Propietarios

        public string AgregarPropietario(Owner propietario)
        {
            var laAccion = new Dominio.Acciones.Propietarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.CrearRegistro(propietario));
            return elResultado;
        }

        public string ActualizarPropietario(Owner propietario)
        {
            var laAccion = new Dominio.Acciones.Propietarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.ActualizarRegistro(propietario));
            return elResultado;
        }

        public string BuscarPropietarioPorCodigo(int codigo)
        {
            var laAccion = new Dominio.Acciones.Propietarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.BuscarRegistroPorCodigo(codigo));
            return elResultado;
        }

        public string ObtenerTodosLosPropietarios()
        {
            var laAccion = new Dominio.Acciones.Propietarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodos());
            return elResultado;
        }

        #endregion

        #region Usuarios

        public string AgregarUsuario(UserInfo usuario)
        {
            var laAccion = new Dominio.Acciones.Usuarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.CrearUsuario(usuario));
            return elResultado;
        }

        public string ActualizarUsuario(UserInfo usuario)
        {
            var laAccion = new Dominio.Acciones.Usuarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.ActualizarRegistro(usuario));
            return elResultado;
        }

        public string BuscarUsuarioPorCodigo(int codigo)
        {
            var laAccion = new Dominio.Acciones.Usuarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.BuscarRegistroPorCodigo(codigo));
            return elResultado;
        }

        public string ObtenerTodosLosUsuarios()
        {
            var laAccion = new Dominio.Acciones.Usuarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodos());
            return elResultado;
        }

        public string ObtenerTodosLosRoles()
        {
            var laAccion = new Dominio.Acciones.Usuarios();
            var elResultado = JsonConvert.SerializeObject(laAccion.ObtenerTodosLosRoles());
            return elResultado;
        }
        #endregion
    }
}
