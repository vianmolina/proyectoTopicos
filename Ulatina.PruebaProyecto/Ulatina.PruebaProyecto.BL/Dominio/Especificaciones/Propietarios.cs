﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.Dominio.Especificaciones
{
    public class Propietarios 
    {
        public DbContext miContexto;
        public Propietarios()
        {

        }
        public Propietarios(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Repositorio.Propietarios ObtenerRepositorio()
        {
            Repositorio.Propietarios result;
            if (miContexto == null)
                result = new Repositorio.Propietarios();
            else
                result = new Repositorio.Propietarios(miContexto);
            return result;
        }

        public Owner AgregarNuevoRegistro(Owner registro)
        {
            Repositorio.Propietarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Agregar(registro);
            return elResultado;
        }
        public Owner ActualizarResgistro(Owner registro)
        {
            Repositorio.Propietarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Actualizar(registro);
            return elResultado;
        }
        public Owner BuscarRegistroPorCodigo(int codigo)
        {
            Repositorio.Propietarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.BuscarPorCodigo(codigo);
            return elResultado;
        }
        public List<Owner> ObtenerTodos()
        {
            Repositorio.Propietarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodos();
            return elResultado;
        }
    }
}