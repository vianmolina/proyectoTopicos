﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.Dominio.Especificaciones
{
    public class Parametros
    {
        public DbContext miContexto;
        public Parametros()
        {

        }
        public Parametros(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Repositorio.Parametros ObtenerRepositorio()
        {
            Repositorio.Parametros result;
            if (miContexto == null)
                result = new Repositorio.Parametros();
            else
                result = new Repositorio.Parametros(miContexto);
            return result;
        }

        public Parameter AgregarNuevoRegistro(Parameter registro)
        {
            Repositorio.Parametros elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Agregar(registro);
            return elResultado;
        }
        public Parameter ActualizarResgistro(Parameter registro)
        {
            Repositorio.Parametros elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Actualizar(registro);
            return elResultado;
        }
        public Parameter BuscarRegistroPorCodigo(int codigo)
        {
            Repositorio.Parametros elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.BuscarPorCodigo(codigo);
            return elResultado;
        }
        public List<Parameter> ObtenerTodos()
        {
            Repositorio.Parametros elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodos();
            return elResultado;
        }
    }
}