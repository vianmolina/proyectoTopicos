﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.Dominio.Especificaciones
{
    public class Etiquetas
    {
        public DbContext miContexto;
        public Etiquetas()
        {

        }
        public Etiquetas(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Repositorio.Etiquetas ObtenerRepositorio()
        {
            Repositorio.Etiquetas result;
            if (miContexto == null)
                result = new Repositorio.Etiquetas();
            else
                result = new Repositorio.Etiquetas(miContexto);
            return result;
        }

        public Tag AgregarNuevoRegistro(Tag registro)
        {
            Repositorio.Etiquetas elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Agregar(registro);
            return elResultado;
        }
        public Tag ActualizarResgistro(Tag registro)
        {
            Repositorio.Etiquetas elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Actualizar(registro);
            return elResultado;
        }
        public Tag BuscarRegistroPorCodigo(int codigo)
        {
            Repositorio.Etiquetas elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.BuscarPorCodigo(codigo);
            return elResultado;
        }
        public List<Tag> ObtenerTodos()
        {
            Repositorio.Etiquetas elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodos();
            return elResultado;
        }
    }
}