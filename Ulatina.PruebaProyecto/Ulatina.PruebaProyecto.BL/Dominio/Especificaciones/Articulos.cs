﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.Dominio.Especificaciones
{
    public class Articulos
    {
        public DbContext miContexto;
        public Articulos()
        {

        }
        public Articulos(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        public Repositorio.Articulos ObtenerRepositorio()
        {
            Repositorio.Articulos result;
            if (miContexto == null)
                result = new Repositorio.Articulos();
            else
                result = new Repositorio.Articulos(miContexto);
            return result;
        }

        public Item AgregarNuevoRegistro(Item registro)
        {
            Repositorio.Articulos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Agregar(registro);
            return elResultado;
        }
        public Item ActualizarResgistro(Item registro)
        {
            Repositorio.Articulos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Actualizar(registro);
            return elResultado;
        }
        public Item BuscarRegistroPorCodigo(int codigo)
        {
            Repositorio.Articulos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.BuscarPorCodigo(codigo);
            return elResultado;
        }
        public List<Item> ObtenerTodos()
        {
            Repositorio.Articulos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodos();
            return elResultado;
        }
    }
}