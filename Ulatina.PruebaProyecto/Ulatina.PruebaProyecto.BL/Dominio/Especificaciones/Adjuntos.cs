﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.Dominio.Especificaciones
{
    public class Adjuntos
    {
        public DbContext miContexto;
        public Adjuntos()
        {

        }
        public Adjuntos(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        //test

        private Repositorio.Adjuntos ObtenerRepositorio()
        {
            Repositorio.Adjuntos result;
            if (miContexto == null)
                result = new Repositorio.Adjuntos();
            else
                result = new Repositorio.Adjuntos(miContexto);
            return result;
        }

        public File AgregarNuevoRegistro(File registro)
        {
            Repositorio.Adjuntos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Agregar(registro);
            return elResultado;
        }
        public File ActualizarResgistro(File registro)
        {
            Repositorio.Adjuntos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Actualizar(registro);
            return elResultado;
        }
        public File BuscarRegistroPorCodigo(int codigo)
        {
            Repositorio.Adjuntos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.BuscarPorCodigo(codigo);
            return elResultado;
        }
        public List<File> ObtenerTodos()
        {
            Repositorio.Adjuntos elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodos();
            return elResultado;
        }
    }
}