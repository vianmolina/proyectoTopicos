﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Ulatina.PruebaProyecto.Model;

namespace Ulatina.PruebaProyecto.Dominio.Especificaciones
{
    public class Usuarios
    {
        public DbContext miContexto;
        public Usuarios()
        {

        }
        public Usuarios(DbContext elContexto)
        {
            miContexto = elContexto;
        }

        private Repositorio.Usuarios ObtenerRepositorio()
        {
            Repositorio.Usuarios result;
            if (miContexto == null)
                result = new Repositorio.Usuarios();
            else
                result = new Repositorio.Usuarios(miContexto);
            return result;
        }

        public UserInfo AgregarNuevoRegistro(UserInfo usuario)
        {
            Repositorio.Usuarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Agregar(usuario);
            return elResultado;
        }
        public UserInfo ActualizarResgistro(UserInfo usuario)
        {
            Repositorio.Usuarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.Actualizar(usuario);
            return elResultado;
        }
        public UserInfo BuscarRegistroPorCodigo(int codigo)
        {
            Repositorio.Usuarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.BuscarPorCodigo(codigo);
            return elResultado;
        }
        public List<UserInfo> ObtenerTodos()
        {
            Repositorio.Usuarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodos();
            return elResultado;
        }
        public List<AspNetRoles> ObtenerTodosLosRoles()
        {
            Repositorio.Usuarios elRepositorio;
            elRepositorio = ObtenerRepositorio();

            var elResultado = elRepositorio.ObtenerTodosLosRoles();
            return elResultado;
        }
    }
}