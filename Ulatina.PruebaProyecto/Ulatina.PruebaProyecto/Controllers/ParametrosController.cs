﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ulatina.PruebaProyecto.Models;

namespace Ulatina.PruebaProyecto.Controllers

{
    public class ParametrosController : Controller
    {
        // GET: Parametros
        public ActionResult Index()
        {
            var items = new List<ParametroMvc>
            {
                //var items = fooRepository.GetAll();
                //return View(items);
                new ParametroMvc() { Codigo = 1, Descripcion = "Item 1", Valor = "123" },
                new ParametroMvc() { Codigo = 1212, Descripcion = "Item 2", Valor = "1234" },
                new ParametroMvc() { Codigo = 4124, Descripcion = "Item 3", Valor = "12345" }
            };
            return View(items);
        }
        public ActionResult Editar(int Codigo)
        {

            return View();
        }
    }
}