﻿using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Ulatina.PruebaProyecto.Models;
using Ulatina.Selectos.Proyecto.UI.WSProyectos;

namespace Ulatina.PruebaProyecto.Controllers
{
    [Authorize(Roles ="Administrador")]
    public class UsuariosController : Controller
    {
        Service1Client ws;

        public UsuariosController()
        {
            ws = new Service1Client();
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Usuarios
        public ActionResult Index()
        {

            List<UserInfo> usuarios = JsonConvert.DeserializeObject<List<UserInfo>>(ws.ObtenerTodosLosUsuarios());
            List<UsuarioMvc> usuarioMvcList = new List<UsuarioMvc>();
            foreach (var usuarioActual in usuarios)
            {
                usuarioMvcList.Add(new UsuarioMvc()
                {
                    Codigo = usuarioActual.Id,
                    Nombre = usuarioActual.FirstName,
                    Apellido = usuarioActual.LastName,
                    Email = usuarioActual.Email,
                    Estado = usuarioActual.Status,
                    Identificacion = usuarioActual.Identification,
                    Telefono = usuarioActual.Telefono
                });
            }
            return View(usuarioMvcList);
        }

        public ActionResult Agregar()
        {

            return View();
        }


        public ActionResult Editar(int codigo)
        {
            UsuarioMvc model = new UsuarioMvc();
            var TodosLosRolesJson = ws.ObtenerTodosLosRoles();
            var resultadoJson = ws.BuscarUsuarioPorCodigo(codigo);
            if (resultadoJson != string.Empty)
            {
                var usuarioActual = JsonConvert.DeserializeObject<UserInfo>(resultadoJson);
                model = new UsuarioMvc()
                {
                    Codigo = usuarioActual.Id,
                    Nombre = usuarioActual.FirstName,
                    Apellido = usuarioActual.LastName,
                    Email = usuarioActual.Email,
                    Estado = usuarioActual.Status,
                    UserId = usuarioActual.IdAspNetUsers,
                    Identificacion = usuarioActual.Identification,
                    Telefono = usuarioActual.Telefono
                };
                var TodosLosRoles = JsonConvert.DeserializeObject<List<AspNetRoles>>(TodosLosRolesJson);
                model.RolesUsuario = new List<RolesMvc>();
                foreach(var role in usuarioActual.UserRoles)
                {
                    model.RolesUsuario.Add(new RolesMvc() { Codigo = role.Id, Descripcion = role.Name });
                    TodosLosRoles.Remove(TodosLosRoles.FirstOrDefault(r=>r.Id == role.Id));
                }
                List<RolesMvc> todosRolesMvc = new List<RolesMvc>();
                foreach (var role in TodosLosRoles)
                {
                    todosRolesMvc.Add(new RolesMvc() { Codigo = role.Id, Descripcion = role.Name });
                }
                ViewBag.TodosRoles = todosRolesMvc;
            }
            return View("EditarUsuario", model);
        }

        public ActionResult Guardar(UsuarioMvc usuario)
        {
            UsuarioMvc model = new UsuarioMvc();
            UserInfo usuarioGuardar = new UserInfo()
            {
                Id = usuario.Codigo,
                FirstName = usuario.Nombre,
                LastName = usuario.Apellido,
                Email = usuario.Email,
                Status = usuario.Estado,
                IdAspNetUsers = usuario.UserId,
                Identification = usuario.Identificacion,
                Telefono = usuario.Telefono
            };
            var resultadoJson = ws.ActualizarUsuario(usuarioGuardar);
  
            return RedirectToAction("Editar", "Usuarios", new { codigo = usuario.Codigo });
        }
        public ActionResult EliminarRole(int codigoUsuario, string role, string userId)
        {
            // var result = await UserManager.RemoveFromRoleAsync(userId, role);
            return RedirectToAction("EliminarRole", "Account", new { codigoUsuario = codigoUsuario, role = role, userId = userId });
            //return RedirectToAction("Editar", new { codigo = codigoUsuario });
        }
        public ActionResult AgregarRole(int codigoUsuario, string role, string userId)
        {
            //var result = await UserManager.AddToRoleAsync(userId, role);
            return RedirectToAction("AgregarRole", "Account", new { codigoUsuario= codigoUsuario, role = role, userId  = userId });
        }
    }
}