﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ulatina.PruebaProyecto.Models
{
    public class UsuarioMvc
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Identificacion { get; set; }
        public bool Estado { get; set; }
        public string UserId { get; set; }
        public List<RolesMvc> RolesUsuario { get; set; }
        public string EstadoText
        {
            get
            {
                return Estado ? "Activo" : "Inactivo";
            }
        }
    }
}