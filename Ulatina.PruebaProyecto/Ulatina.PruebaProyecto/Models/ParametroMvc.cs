﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ulatina.PruebaProyecto.Models
{
    public class ParametroMvc
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public int CodigoPadre { get; set; }
        public string DescripcionPadre { get; set; }
        public bool Estado { get; set; }
    }
}