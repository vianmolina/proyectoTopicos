﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ulatina.PruebaProyecto.Models
{
    public class RolesMvc
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}