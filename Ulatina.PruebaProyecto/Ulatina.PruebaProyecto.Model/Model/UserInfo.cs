﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Ulatina.Selectos.Proyecto.GenericRepository.Model.Dominio.Tablas;

namespace Ulatina.Selectos.Proyecto.Model
{
    public partial class UserInfo : Entity<UserInfo>
    {
        [DataMember]
        public List<AspNetRoles> UserRoles { get {

                return this.AspNetUsers== null? null:this.AspNetUsers.AspNetRoles.ToList();

            } }
    }
}
